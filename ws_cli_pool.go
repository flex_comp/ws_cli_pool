package ws_cli_pool

import (
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf"
	"net/http"
	"net/url"
	"time"
)

var (
	wcp *WSCliPool
)

type WSCliPool struct{}

func init() {
	wcp = new(WSCliPool)
	_ = comp.RegComp(wcp)
}

func (w *WSCliPool) Init(_ map[string]interface{}, _ ...interface{}) error {
	return nil
}

func (w *WSCliPool) Start(_ ...interface{}) error {
	return nil
}

func (w *WSCliPool) UnInit() {

}

func (w *WSCliPool) Name() string {
	return "ws-cli-pool"
}

func Create(url url.URL, header http.Header, subProtocol ...string) (*WSClient, error) {
	return wcp.Create(url, header, subProtocol...)
}

func (w *WSCliPool) Create(url url.URL, header http.Header, subProtocol ...string) (*WSClient, error) {
	dia := &websocket.Dialer{
		Proxy:            http.ProxyFromEnvironment,
		HandshakeTimeout: 15 * time.Second,
		Subprotocols:     subProtocol,
	}

	conn, _, err := dia.Dial(url.String(), header)
	if err != nil {
		return nil, err
	}

	cli := NewWSClient()
	cli.conn = conn

	chClose := make(chan bool, 1)
	go readPump(cli, chClose)
	go writePump(cli, chClose)
	return cli, nil
}

func readPump(cli *WSClient, chClose chan<- bool) {
	defer func() {
		select {
		case chClose <- true:
		default:
		}
	}()

	for {
		ty, data, err := cli.conn.ReadMessage()
		if err != nil {
			log.Error("Websocket数据读取错误, 断开连接, IP:", cli.conn.RemoteAddr(), " 错误:", err)
			return
		}

		if ty == websocket.TextMessage {
			continue
		}

		// 从客户端获取的Client ID在连接时已验证,忽略消息内信息
		msg, err := protobuf.UnMarshalRspRaw(data)
		if err != nil {
			log.Error("Websocket数据解析错误, IP:", cli.conn.RemoteAddr(), " 错误:", err)
			continue
		}

		cli.connMessageMtx.RLock()
		m, ok := cli.connMessage[msg.Head]
		if !ok {
			// 消息未被监听处理, 直接任未处理中
			cli.unHandleMessageMtx.RLock()
			for _, messages := range cli.unHandleMessage {
				select {
				case messages <- msg:
				default:
					log.Warn("通道阻塞")
				}

			}
			cli.unHandleMessageMtx.RUnlock()
		} else {
			for _, messages := range m {
				select {
				case messages <- msg:
				default:
					log.Warn("通道阻塞")
				}
			}
		}

		cli.connMessageMtx.RUnlock()
	}
}

func writePump(cli *WSClient, chClose <-chan bool) {
	defer func() {
		for _, ch := range cli.close {
			select {
			case ch <- true:
			default:
			}
		}
	}()

	for {
		select {
		case <-chClose:
			return
		case <-cli.done:
			return
		case raw := <-cli.sender:
			err := cli.conn.WriteMessage(websocket.BinaryMessage, raw)
			if err != nil {
				log.Error("发送消息失败:", err)
				return
			}
		}
	}
}
