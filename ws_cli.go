package ws_cli_pool

import (
	"github.com/gorilla/websocket"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf"
	"gitlab.com/flex_comp/uid"
	"google.golang.org/protobuf/proto"
	"sync"
)

type WSClient struct {
	conn *websocket.Conn

	connMessage    map[string]map[int64]chan *protobuf.Response // [head][uid]消息
	connMessageMtx sync.RWMutex

	unHandleMessage    map[int64]chan *protobuf.Response // [uid]未处理消息
	unHandleMessageMtx sync.RWMutex

	sender chan []byte

	close    map[int64]chan bool // 关闭信号
	closeMtx sync.RWMutex

	done chan bool // 手动关闭
}

func NewWSClient() *WSClient {
	return &WSClient{
		connMessage:     make(map[string]map[int64]chan *protobuf.Response),
		unHandleMessage: make(map[int64]chan *protobuf.Response),
		sender:          make(chan []byte, 4096),
		close:           make(map[int64]chan bool),
		done:            make(chan bool, 1),
	}
}

func (c *WSClient) Close() {
	select {
	case c.done <- true:
	default:
		log.Warn("通道阻塞")
	}
}

func (c *WSClient) Send(cliId string, seq uint64, msg proto.Message, gSrvId uint32) bool {
	req := &protobuf.Request{
		CmdNo:        seq,
		ClientID:     cliId,
		GameServerID: gSrvId,
	}

	data, err := protobuf.MarshalReq(req, msg)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return false
	}

	select {
	case c.sender <- data:
	default:
		log.Warn("通道阻塞")
	}

	return true
}

func (c *WSClient) OnMessage(head string) (chan *protobuf.Response, int64) {
	id := uid.Gen()
	ch := make(chan *protobuf.Response, 1024)

	c.connMessageMtx.Lock()
	defer c.connMessageMtx.Unlock()

	m, ok := c.connMessage[head]
	if !ok {
		m = make(map[int64]chan *protobuf.Response)
		c.connMessage[head] = m
	}

	m[id] = ch
	return ch, id
}

func (c *WSClient) OnUnHandleMessage() (chan *protobuf.Response, int64) {
	id := uid.Gen()
	ch := make(chan *protobuf.Response, 10240)

	c.unHandleMessageMtx.Lock()
	defer c.unHandleMessageMtx.Unlock()

	c.unHandleMessage[id] = ch
	return ch, id
}

func (c *WSClient) OnClose() (chan bool, int64) {
	id := uid.Gen()
	ch := make(chan bool, 1)

	c.closeMtx.Lock()
	defer c.closeMtx.Unlock()

	c.close[id] = ch
	return ch, id
}
