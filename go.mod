module gitlab.com/flex_comp/ws_cli_pool

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e
	gitlab.com/flex_comp/protobuf v0.0.0-20210730165512-8202d859a513
	gitlab.com/flex_comp/uid v0.0.0-20210729183622-f1ddefa50772
	google.golang.org/protobuf v1.27.1
)
